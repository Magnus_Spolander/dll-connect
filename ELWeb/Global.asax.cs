﻿using ELWeb.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;
using WebAPITest.Controllers;
using WebAPITest.Models;

namespace WebAPITest
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("fr-FR");
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            log4net.Config.XmlConfigurator.Configure();
            ELWeb.Helpers.Logger.GetLogger(typeof(GlobalConfiguration)).Debug("Starting application...");


            //Accept all certificates - Only for testing purposes with Entré's test environment (Lion03)
            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
        }

        public override void Init()
        {
            this.PostAuthenticateRequest += MvcApplication_PostAuthenticateRequest;
            base.Init();
        }

        void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.SetSessionStateBehavior(
                SessionStateBehavior.Required);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value != null)
            {
                try
                {
                    var current = CultureInfo.CreateSpecificCulture(Request.Cookies.Get("culture").Value);
                    if (current != null)
                    {
                        Thread.CurrentThread.CurrentCulture = current;
                        Thread.CurrentThread.CurrentUICulture = current;
                    }

                }
                catch (Exception)
                {
                    Logger.GetLogger(typeof(GlobalConfiguration)).ErrorFormat("Failed to set current culture to {0}", Request.Cookies.Get("culture").Value);
                }
            }
        }

    }
}