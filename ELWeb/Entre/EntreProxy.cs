﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using WebAPITest.Extensions;
using WebAPITest.Entre;
using System.Web.Script.Serialization;
using System.Net.Http;
using WebAPITest.Models;
using Newtonsoft.Json.Linq;
using WebAPITest.Helpers;
using System.Configuration;
using ELWeb.Helpers;
using Newtonsoft.Json;
using System.Web.Security;
using System.Web.Mvc;

namespace WebAPITest.Entre
{
    public class EntreProxy
    {
        HttpClient client;

        public EntreProxy()
        {
            client = HttpClientSingleton.Instance;
        }

        public EntreMessage<LoginInfo> Login(object loginData)
        {
            EntreMessage<LoginInfo> response;
            HttpResponseMessage result;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).Debug("Login in to Entre...");
                
                //var result = client.PostAsJsonAsync<object>("https://testentre.delagelanden.com/entre/common/entre_light.nsf/login?OpenAgent", loginData).Result;
                result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["EntreBaseAddress"] + ConfigurationManager.AppSettings["LoginAddress"], loginData).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to log in to Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<LoginInfo>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Failed to log in to Entre!");
                    response = new EntreMessage<LoginInfo>() { code = (int)result.StatusCode, message = "Failed to log in to Entre! \r\n" + result.ReasonPhrase, body = null };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error("Failed to log in to Entre!", e);
                if (e.InnerException.InnerException.Message == "Unable to connect to the remote server")
                {
                    response = new EntreMessage<LoginInfo>() { code = 408, message = "Failed to log in to Entre! \r\n The server is not available at the moment. Please try again later" }; 
                }
                else
                {
                    response = new EntreMessage<LoginInfo>() { code = 999, message = "Unknown error", body = null };
                }                
            }
            return response;
        }

        public EntreMessage<PartnerData> GetPartnerData(object session)
        {
            EntreMessage<PartnerData> response;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).Debug("Getting partner data from Entre...");
                //var result = client.PostAsJsonAsync<object>("https://testentre.delagelanden.com/entre/common/entre_light.nsf/getPartnerData?OpenAgent", session).Result;
                var result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["GetPartnerDataAddress"], session).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to get partner data from Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<PartnerData>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Failed to get partner data from Entre! - {0}", result.ReasonPhrase);
                    response = new EntreMessage<PartnerData>() { code = (int)result.StatusCode, message = result.ReasonPhrase, body = null };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error("Failed to get partner data from Entre!", e);
                response = new EntreMessage<PartnerData>() { code = 999, message = "Unknown error", body = null };
            }
            return response;
        }


        public EntreMessage<CalculationResult> Calculate(object message)
        {
            EntreMessage<CalculationResult> response;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).Debug("Calculating in Entre...");
                //var result = client.PostAsJsonAsync<object>("https://85.119.135.8/entre/common/entre_light.nsf/calculate?OpenAgent", message).Result;
                var result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["CalculateAddress"], message).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to calculate in Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<CalculationResult>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Failed to calculate in Entre! - {0}", result.ReasonPhrase);
                    response = new EntreMessage<CalculationResult>() { code = (int)result.StatusCode, message = result.ReasonPhrase, body = null };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error("Failed to calculate in Entre!", e);
                response = new EntreMessage<CalculationResult>() { code = 999, message = "Unknown error", body = null };
            }
            return response;
        }

        public EntreMessage<JObject> SaveCalculation(object message)
        {
            EntreMessage<JObject> response;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).Debug("Saving calculations in Entre...");
                //var result = client.PostAsJsonAsync<object>("https://85.119.135.8/entre/common/entre_light.nsf/saveCalculation?OpenAgent", message).Result;
                var result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["SaveCalculationAddress"], message).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to save calculations in Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<JObject>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Failed to save calculations in Entre! - {0}", result.ReasonPhrase);
                    response = new EntreMessage<JObject>() { code = (int)result.StatusCode, message = result.ReasonPhrase, body = null };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error("Failed to save calculations in Entre!", e);
                response = new EntreMessage<JObject>() { code = 999, message = "Unknown error", body = null };
            }
            return response;
        }

        public EntreMessage<JObject> GetCustomer(object message)
        {
            EntreMessage<JObject> response;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).Debug("Getting customer from Entre...");
                //var result = client.PostAsJsonAsync<object>("https://85.119.135.8/entre/common/entre_light.nsf/getCustomer?OpenAgent", message).Result;
                var result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["GetCustomerAddress"], message).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to get customer from Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<JObject>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Failed to get customer from Entre! - {0}");
                    response = new EntreMessage<JObject>() { code = (int)result.StatusCode, message = result.ReasonPhrase, body = null };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error("Failed to get customer from Entre!", e);
                response = new EntreMessage<JObject>() { code = 999, message = "Unknown error", body = null };
            }
            return response;
        }

        public EntreMessage<JObject> SaveCustomer(object message)
        {
            EntreMessage<JObject> response;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).Debug("Saving customer in Entre...");
                //var result = client.PostAsJsonAsync<object>("https://85.119.135.8/entre/common/entre_light.nsf/saveCustomer?OpenAgent", message).Result;
                var result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["SaveCustomerAddress"], message).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to save customer in Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<JObject>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Failed to save customer in Entre! - {0}", result.ReasonPhrase);
                    response = new EntreMessage<JObject>() { code = (int)result.StatusCode, message = result.ReasonPhrase, body = null };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error("Failed to save customer in Entre!", e);
                response = new EntreMessage<JObject>() { code = 999, message = "Unknown error", body = null };
            }
            return response;
        }

        public EntreMessage<JObject> SendCreditRequest(object message)
        {
            EntreMessage<JObject> response;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).Debug("Sending credit request to Entre...");
                //var result = client.PostAsJsonAsync<object>("https://85.119.135.8/entre/common/entre_light.nsf/sendCreditRequest?OpenAgent", message).Result;
                var result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["SendCreditRequestAddress"], message).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to send credit request to Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<JObject>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Failed to send credit request to Entre! - {0}", result.ReasonPhrase);
                    response = new EntreMessage<JObject>() { code = (int)result.StatusCode, message = result.ReasonPhrase, body = null };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error("Failed to send credit request to Entre!", e);
                response = new EntreMessage<JObject>() { code = 999, message = "Unknown error", body = null };
            }
            return response;
        }

        public EntreMessage<JObject> GetAgreementData(object message)
        {
            EntreMessage<JObject> response;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).Debug("Getting agreement data from Entre...");
                //var result = client.PostAsJsonAsync<object>("https://85.119.135.8/entre/common/entre_light.nsf/getAgreementData?OpenAgent", message).Result;
                var result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["GetAgreementDataAddress"], message).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to get agreement data from Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<JObject>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Failed to get agreement data from Entre! - {0}", result.ReasonPhrase);
                    response = new EntreMessage<JObject>() { code = (int)result.StatusCode, message = result.ReasonPhrase, body = null };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error("Failed to get agreement data from Entre!", e);
                response = new EntreMessage<JObject>() { code = 999, message = "Unknown error", body = null };
            }
            return response;
        }

        public EntreMessage<JObject> CreateAgreement(object message)
        {
            EntreMessage<JObject> response;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).Debug("Creating agreement in Entre...");
                //var result = client.PostAsJsonAsync<object>("https://85.119.135.8/entre/common/entre_light.nsf/createAgreement?OpenAgent", message).Result;
                var result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["CreateAgreementAddress"], message).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to create agreement in Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<JObject>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Failed to create agreement in Entre! - {0}", result.ReasonPhrase);
                    response = new EntreMessage<JObject>() { code = (int)result.StatusCode, message = result.ReasonPhrase, body = null };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error("Failed to create agreement in Entre!", e);
                response = new EntreMessage<JObject>() { code = 999, message = "Unknown error", body = null };
            }
            return response;
        }

        public EntreMessage<string> GetAgreementDocument(object message)
        {
            EntreMessage<string> response;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).Debug("Getting agreement document from Entre...");
                //var result = client.PostAsJsonAsync<object>("https://85.119.135.8/entre/common/entre_light.nsf/getAgreementDocument?OpenAgent", message).Result;
                var result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["GetAgreementDocumentAddress"], message).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to get agreement document from Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<string>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Failed to get agreement document from Entre! - {0}", result.ReasonPhrase);
                    response = new EntreMessage<string>() { code = (int)result.StatusCode, message = result.ReasonPhrase, body = "" };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error("Failed to get agreement document from Entre!", e);
                response = new EntreMessage<string>() { code = 999, message = "Unknown error", body = "" };
            }
            return response;
        }

        public EntreMessage<JObject> GetDealByReferenceNo(object message)
        {
            EntreMessage<JObject> response;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Getting deal by reference '{0}' from Entre...", message);
                //var result = client.PostAsJsonAsync<object>("https://85.119.135.8/entre/common/entre_light.nsf/getDealByReferenceNo?OpenAgent", message).Result;
                var result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["GetDealByReferenceNoAddress"], message).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to get deal by reference from Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<JObject>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Failed to get deal by reference from Entre! - {0}", result.ReasonPhrase);
                    response = new EntreMessage<JObject>() { code = (int)result.StatusCode, message = result.ReasonPhrase, body = null };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error(string.Format("Failed to get deal by reference number '{0}'", message.ToString()), e);
                response = new EntreMessage<JObject>() { code = 999, message = "Unknown error", body = null };
            }
            return response;
        }

        public EntreMessage<JObject> SendEsign(object message)
        {
            EntreMessage<JObject> response;
            try
            {
                Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Sending esign email(s)...");
                //var result = client.PostAsJsonAsync<object>("https://85.119.135.8/entre/common/entre_light.nsf/sendEsign?OpenAgent", message).Result;
                var result = client.PostAsJsonAsync<object>(ConfigurationManager.AppSettings["SendEsign"], message).Result;
                if (result.IsSuccessStatusCode)
                {
                    Logger.GetLogger(typeof(EntreProxy)).Debug("Succeeded to send esign email(s) from Entre!");
                    response = result.Content.ReadAsAsync<EntreMessage<JObject>>().Result;
                }
                else
                {
                    Logger.GetLogger(typeof(EntreProxy)).DebugFormat("Failed to send esign email(s) from Entre! - {0}", result.ReasonPhrase);
                    response = new EntreMessage<JObject>() { code = (int)result.StatusCode, message = result.ReasonPhrase, body = null };
                }
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(EntreProxy)).Error(string.Format("Failed to send esign email(s) to '{0}'", message.ToString()), e);
                response = new EntreMessage<JObject>() { code = 999, message = "Unknown error", body = null };
            }
            return response;
        }

    }
}