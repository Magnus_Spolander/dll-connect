﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace WebAPITest.Entre
{
    public class EntreMessage<T>
    {
        public string message { get; set; }
        public int code { get; set; }
        public T body { get; set; }
    }
}
