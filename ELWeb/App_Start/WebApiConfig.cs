﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebAPITest.Handlers;
using WebAPITest.Attributes;

namespace WebAPITest
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApiGet",
            //    routeTemplate: "{controller}/{id}",
            //    defaults: new { controller = "Values", action = "Get", id = RouteParameter.Optional }
            //    //defaults: new { id = RouteParameter.Optional }
            //);

            config.Routes.MapHttpRoute(
                name: "DefaultApiPost",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { controller = "Entre", action = "Post" }
            );
            //config.Filters.Add(new CustomAuthorizeAttribute());
            //config.MessageHandlers.Add(new CustomAuthenticationHandler());
        }
    }
}
