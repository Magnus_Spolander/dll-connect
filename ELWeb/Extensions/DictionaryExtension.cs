﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPITest.Extensions
{
    public static class DictionaryExtension
    {
        public static string ToQueryString(this Dictionary<string, string> dict)
        {
            return '?' + string.Join("&", dict.Select(p => p.Key + '=' + p.Value).ToArray());
        }
    }
}