﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPITest.Extensions
{
    [Serializable]
    public class EntreLightWebError : System.Exception
    {
        public EntreLightWebError()
        {
        }

        public EntreLightWebError(string message)
            : base(message)
        {
        }

        public EntreLightWebError(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}