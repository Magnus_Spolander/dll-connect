﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Resources;
using System.Collections;
using System.Globalization;
using System.Threading;

namespace WebAPITest.Extensions
{
	public static class ResourceExtension
    {
        public static string GetLocalizedText(this string inStr,
            string txtPrefix)
        {
            CultureInfo current = Thread.CurrentThread.CurrentCulture;
            string strResource = Resources.LanguageTemplate.ResourceManager.GetString(txtPrefix + inStr);

            if (strResource == null)
            {                
                Hashtable data = new Hashtable();
                data.Add(txtPrefix + inStr, inStr);
                UpdateResourceFile(data);
            }

            return Resources.LanguageTemplate.ResourceManager.GetString(txtPrefix + inStr);
        }

        public static string GetLocalizedEnumText(this string inStr,
            string txtPrefix)
        {
			//txtPrefix = "Enum_";
			string strResource = Resources.LanguageTemplate.ResourceManager.GetString(txtPrefix + inStr);

            if (strResource == null)
            {
                Hashtable data = new Hashtable();
                data.Add(txtPrefix + inStr, inStr);
                UpdateResourceFile(data);
            }

            return Resources.LanguageTemplate.ResourceManager.GetString(txtPrefix + inStr);
        }

        public static void UpdateResourceFile(Hashtable data)
        {
			

            string sResxPath = AppDomain.CurrentDomain.BaseDirectory + @"App_GlobalResources\LanguageTemplate.resx";
            Hashtable resourceEntries = new Hashtable();
            //Get existing resources     
            
            ResXResourceReader reader = new ResXResourceReader(sResxPath);
            if (reader != null)
            {
                IDictionaryEnumerator id = reader.GetEnumerator();
                foreach (DictionaryEntry d in reader)
                {
                    if (d.Value == null)
                        resourceEntries.Add(d.Key.ToString(), "");
                    else
                        resourceEntries.Add(d.Key.ToString(), d.Value.ToString());
                } reader.Close();
            }
            //Modify resources here...    
            foreach (String key in data.Keys)
            {
                if (!resourceEntries.ContainsKey(key))
                {
                    String value = data[key].ToString();
                    if (value == null)
                        value = "";
                    resourceEntries.Add(key, value);
                }
                else
                {
                    String value = data[key].ToString();
                    if (value == null)
                        value = "";
                    resourceEntries.Remove(key);
                    resourceEntries.Add(key, data[key].ToString());
                }
            }
            //Write the combined resource file     
            ResXResourceWriter resourceWriter = new ResXResourceWriter(sResxPath);
            foreach (String key in resourceEntries.Keys)
            {
                resourceWriter.AddResource(key, resourceEntries[key]);
            }
            resourceWriter.Generate();
            resourceWriter.Close();
        }
    }
}