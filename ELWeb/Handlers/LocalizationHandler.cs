﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Http.Headers;
using System.Diagnostics;
using System.Globalization;

namespace WebAPITest.Handlers
{
    public class CustomAuthenticationHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            CookieHeaderValue cookie = request.Headers.GetCookies("culture").FirstOrDefault();
            if (cookie != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cookie["cultue"].Value);
            }
            return base.SendAsync(request, cancellationToken);
        }
    }
}