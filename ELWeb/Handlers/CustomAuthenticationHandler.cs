﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Http.Headers;
using System.Diagnostics;
using System.Globalization;

namespace WebAPITest.Handlers
{
    public class LocalizationHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.Content.IsFormData())
            {
                string s = request.Content.ReadAsStringAsync().Result;
                Debug.WriteLine(s);
            }
            string sessionId = "";
            CookieHeaderValue cookie = request.Headers.GetCookies("session-id").FirstOrDefault();
            if (cookie != null)
            {
                sessionId = cookie["session-id"].Value;
            }
            else
            {
                return Task.Factory.StartNew(() =>
                {
                    return new HttpResponseMessage(HttpStatusCode.Forbidden)
                    {
                        Content = new StringContent("Unauthorized User")
                    };
                });
            }
            return base.SendAsync(request, cancellationToken);
        }
    }
}