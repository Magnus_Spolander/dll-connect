﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebAPITest.Models
{
    public class CustomerInfo
    {
        public string contact { get; set; }
        public string phoneNo { get; set; }
        public string email { get; set; }
        public string fax { get; set; }
    }
}
