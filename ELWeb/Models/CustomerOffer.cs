﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebAPITest.Models
{
    public class CustomerOffer
    {
        public string name { get; set; }

        public string key { get; set; }

        public EditableItem<int> duration { get; set; }
        //public IEnumerable<int> DurationOptions { get; set; }

        public EditableItem<bool> inAdvance { get; set; }
        //public IEnumerable<string> BillingOptions { get; set; }

        public EditableItem<string> interestType { get; set; }
        //public IEnumerable<string> InterestTypeOptions { get; set; }

        public EditableItem<int> periodLength { get; set; }
        //public IEnumerable<int> PeriodLengthOptions { get; set; }

        public EditableItem<string> residualClause { get; set; }
        //public IEnumerable<string> ResidualClauseOptions { get; set; }

        public IEnumerable<ProductSection> sectionProducts { get; set; }

        public string currency { get; set; }

        public decimal salesCommissionMax { get; set; }

        public int issueFee { get; set; }
    }
}
