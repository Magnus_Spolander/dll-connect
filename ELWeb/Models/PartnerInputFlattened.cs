﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace WebAPITest.Models
{
    public class PartnerInputAllStrings
    {
        public string username { get; set; }
        public string password { get; set; }
        public string customerOfferID { get; set; }
        public string salesPersonEmail { get; set; }
        public string billing { get; set; }
        public string interestType { get; set; }
        public string periodLength { get; set; }
        public string duration { get; set; }
        public string residualClause { get; set; }
        public string salesCommission { get; set; }
        public string externalReference { get; set; }
        public string sendCreditRequest { get; set; }
        public string customerOrgNo { get; set; }
        public InvoiceAddress invoiceAddress { get; set; }
        public InstallationAddress installationAddress { get; set; }
        public CustomerInfo customerInfo { get; set; }
        public string calculateByRent { get; set; }
        public IEnumerable<ProductRowAllStrings> products { get; set; }
        public string smeFinancing { get; set; }


        private List<string> _errors;
        public List<string> Errors
        {
            get
            {
                if (_errors == null)
                {
                    _errors = new List<string>();
                }
                return _errors;
            }
            set
            {
                _errors = value;
            }
        }
        private List<string> _warnings;
        public List<string> Warnings
        {
            get
            {
                if (_warnings == null)
                {
                    _warnings = new List<string>();
                }
                return _warnings;
            }
            set
            {
                _warnings = value;
            }
        }

    }

    public class ProductRowAllStrings
    {
        public string type { get; set; }
        public string name { get; set; }
        public string specification { get; set; }
        public string quantity { get; set; }
        public string price { get; set; }
        public string rent { get; set; }
    }
}