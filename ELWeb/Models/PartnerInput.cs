﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace WebAPITest.Models
{
    public class PartnerInput
    {
        public string username { get; set; }
        public string password { get; set; }
        public string customerOfferID { get; set; }
        public string salesPersonEmail { get; set; }
        public string billing { get; set; }
        public string interestType { get; set; }
        public int periodLength { get; set; }
        public int duration { get; set; }
        public string residualClause { get; set; }
        public string externalReference { get; set; }
        public bool sendCreditRequest { get; set; }
        public string customerOrgNo { get; set; }
        public bool calculateByRent { get; set; }
        public int salesCommission { get; set; }
        public bool smeFinancing { get; set; }
        public int noOfEmployees { get; set; }
        public InvoiceAddress invoiceAddress { get; set; }
        public InstallationAddress installationAddress { get; set; }
        public CustomerInfo customerInfo { get; set; }
        public IEnumerable<ProductRow> products { get; set; }
        public string countryCode { get; set; }
       

        public static PartnerInput Deserialize(String json)
        {
            PartnerInput pi = JsonConvert.DeserializeObject<PartnerInput>(json);

            //Price and rent are decimal type to handle decimals, but Entré doesn't allow decimals so we need to round them to integer.
            if (pi.products != null)
            {
                foreach (ProductRow pr in pi.products)
                {
                    pr.price = Math.Round(pr.price, 0);
                    pr.rent = Math.Round(pr.rent, 0);
                }
            }


            return pi;
        }


        private List<string> _errors;
        public List<string> Errors
        {
            get
            {
                return _errors ?? (_errors = new List<string>());
            }
            set
            {
                _errors = value;
            }
        }
        private List<string> _warnings;
        public List<string> Warnings
        {
            get
            {
                return _warnings ?? (_warnings = new List<string>());
            }
            set
            {
                _warnings = value;
            }
        }

    }
}