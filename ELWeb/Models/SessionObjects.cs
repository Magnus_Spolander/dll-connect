﻿using ELWeb.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace WebAPITest.Models
{
    public class SessionObjects
    {
        public static void ClearAllSessionData()
        {
            HttpContext.Current.Session["EntreLight.WEB.CustomerInfo"] = null;
            HttpContext.Current.Session["EntreLight.WEB.SalesPersonInfo"] = null;
            HttpContext.Current.Session["EntreLight.WEB.CurrentSupportInfo"] = null;
            HttpContext.Current.Session["EntreLight.WEB.InvoiceAddressInfo"] = null;
            HttpContext.Current.Session["EntreLight.WEB.SendToManual"] = null;
            HttpContext.Current.Session["EntreLight.WEB.SendToManualEditable"] = null;
            HttpContext.Current.Session["EntreLight.WEB.EntreDealNumber"] = null;
            HttpContext.Current.Session["EntreLight.WEB.PartnerOrderNumber"] = null;
            HttpContext.Current.Session["InstallationAddressLocation"] = null;
            HttpContext.Current.Session["EntreLight.WEB.CurrentUserName"] = null;
        }

        public static CultureInfo currentCulture { get; set; }

        public static string inputData { get; set; }

        public static CustomerInfo CurrentCustomerInfo 
        { 
            get
            {
                if (HttpContext.Current.Session == null)
                    return null;
                if (HttpContext.Current.Session["EntreLight.WEB.CustomerInfo"] == null)
                    HttpContext.Current.Session["EntreLight.WEB.CustomerInfo"] = new CustomerInfo();  

                return HttpContext.Current.Session["EntreLight.WEB.CustomerInfo"] as CustomerInfo;
            } 
            set 
            {
                if (value == null)
	            {
                    Logger.GetLogger(typeof(SessionObjects)).Debug("Current customer info being set to null");
	            }

                HttpContext.Current.Session["EntreLight.WEB.CustomerInfo"] = value;
            }
        }

        public static CustomerInfo CurrentSalesPersonInfo
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return null;
                if (HttpContext.Current.Session["EntreLight.WEB.SalesPersonInfo"] == null)
                    HttpContext.Current.Session["EntreLight.WEB.SalesPersonInfo"] = new CustomerInfo();  

                return HttpContext.Current.Session["EntreLight.WEB.SalesPersonInfo"] as CustomerInfo;
            }
            set
            {
                if (value == null)
                {
                    Logger.GetLogger(typeof(SessionObjects)).Debug("Current sales person info being set to null");
                }

                HttpContext.Current.Session["EntreLight.WEB.SalesPersonInfo"] = value;
            }
        }

        public static CustomerInfo CurrentSupportInfo
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return null;
                if ( HttpContext.Current.Session["EntreLight.WEB.CurrentSupportInfo"] == null)
                    HttpContext.Current.Session["EntreLight.WEB.CurrentSupportInfo"] = new CustomerInfo();                
                
                return HttpContext.Current.Session["EntreLight.WEB.CurrentSupportInfo"] as CustomerInfo;
            }
            set
            {
                if (value == null)
                {
                    Logger.GetLogger(typeof(SessionObjects)).Debug("Current support info being set to null");
                }

                HttpContext.Current.Session["EntreLight.WEB.CurrentSupportInfo"] = value;
            }
        }

        public static Customer InvoiceAddressInfo
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return null;
                if (HttpContext.Current.Session["EntreLight.WEB.InvoiceAddressInfo"] == null)
                    HttpContext.Current.Session["EntreLight.WEB.InvoiceAddressInfo"] = new Customer();

                return HttpContext.Current.Session["EntreLight.WEB.InvoiceAddressInfo"] as Customer;
            }
            set
            {
                if (value == null)
                {
                    Logger.GetLogger(typeof(SessionObjects)).Debug("Current invoice address info being set to null");
                }

                HttpContext.Current.Session["EntreLight.WEB.InvoiceAddressInfo"] = value;
            }
        }

        public static bool SendToManual
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return false;

                return (bool)HttpContext.Current.Session["EntreLight.WEB.SendToManual"];
            }
            set
            {
                HttpContext.Current.Session["EntreLight.WEB.SendToManual"] = value;
            }
        }

        public static bool SendToManualEditable
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return false;

                return (bool)HttpContext.Current.Session["EntreLight.WEB.SendToManualEditable"];
            }
            set
            {
                HttpContext.Current.Session["EntreLight.WEB.SendToManualEditable"] = value;
            }
        }

        public static string EntreDealNumber
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return null;

                return HttpContext.Current.Session["EntreLight.WEB.EntreDealNumber"] as string;
            }
            set
            {
                HttpContext.Current.Session["EntreLight.WEB.EntreDealNumber"] = value;
            }
        }
        public static string PartnerOrderNumber
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return null;

                return HttpContext.Current.Session["EntreLight.WEB.PartnerOrderNumber"] as string;
            }
            set
            {
                HttpContext.Current.Session["EntreLight.WEB.PartnerOrderNumber"] = value;
            }
        }

        public static string InstallationAddress
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return null;

                return HttpContext.Current.Session["EntreLight.WEB.InstallationAddress"] as string;
            }
            set
            {
                HttpContext.Current.Session["EntreLight.WEB.InstallationAddress"] = value;
            }
        }
        public static string InstallationAddressLocation
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return null;

                return HttpContext.Current.Session["EntreLight.WEB.InstallationAddressLocation"] as string;
            }
            set
            {
                HttpContext.Current.Session["EntreLight.WEB.InstallationAddressLocation"] = value;
            }
        }

        public static string CurrentUserName
        {
            get
            {
                if (HttpContext.Current.Session == null)
                    return null;

                return HttpContext.Current.Session["EntreLight.WEB.CurrentUserName"] as string;
            }
            set
            {
                HttpContext.Current.Session["EntreLight.WEB.CurrentUserName"] = value;
            }
        }


        // Get sessionid from the forms authentication cookie
        public static string CurrentSessionId
        {
            get
            {
                HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];                           
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                // make sure this is the same user
                if (authTicket.Name == SessionObjects.CurrentUserName)
                {
                    return authTicket.UserData;
                }
                throw new Exception("Current user does not match with Entre session");
            }
        }
    }
}