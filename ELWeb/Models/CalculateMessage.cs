﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPITest.Models
{
    public class CalculateMessage
    {
        public string externalSessionID { get; set; }
        public string dealID { get; set; }
        public Calculation calculationData { get; set; }

        public string customerOfferKey { get; set; }
    }
}