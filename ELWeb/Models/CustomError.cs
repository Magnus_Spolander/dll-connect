﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebAPITest.Models
{
    public class CustomError
    {
        public string ErrorTitle { get; set; }
        public string ErrorMessage { get; set; }
    }
}
