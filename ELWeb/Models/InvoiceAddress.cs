﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebAPITest.Models
{
    public class InvoiceAddress
    {
        public string coName { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string zipCode { get; set; }
    }
}
