﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebAPITest.Models
{
    public class AgreementIndexViewModel
    {
        public string dealID { get; set; }
        public string externalReference { get; set; }

        public string creditDecisionCode { get; set; }
        public string creditReasonCode { get; set; }
        public string creditDecisionText { get; set; }
        public string creditDecisionDate { get; set; }

        public string specification { get; set; }
        public IEnumerable<string> specialConditions { get; set; }
        public IEnumerable<string> insuranceOptions { get; set; }

        public string customerContact { get; set; }
        public string customerPhone { get; set; }
        public string customerEmail { get; set; }
        public string customerFax { get; set; }

        public string invoiceCoName { get; set; }
        public string invoiceAddress { get; set; }
        public string invoiceCity { get; set; }
        public string invoiceZipCode { get; set; }

        public string installationAddress { get; set; }
        public string installationLocation { get; set; }
               
        public string esignEmail { get; set; }
        public string esignAgreementEnabled { get; set; }
        public string esignDeliveryApprovalEnabled { get; set; }
    }
}