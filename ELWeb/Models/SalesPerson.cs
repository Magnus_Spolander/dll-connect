﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebAPITest.Models
{
    public class SalesPerson
    {
        public string fullName { get; set; }
        public string emailAddress { get; set; }
    }
}
