﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPITest.Models;

namespace WebAPITest.Models
{
    public class PartnerData
    {
        public string key { get; set; }
        public string countryCode { get; set; }
        public IEnumerable<SalesPerson> salesPersons { get; set; }
        public IEnumerable<CustomerOffer> customerOffers { get; set; }
        public SalesContact salesContact { get; set; }
        public EditableItem<bool> sendCreditToManual { get; set; }
        public bool smeFinancingEnabled { get; set; }
    }
}