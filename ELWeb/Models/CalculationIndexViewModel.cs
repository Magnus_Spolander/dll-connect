﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPITest.Models
{
    public class CalculationIndexViewModel
    {
        public string dealID { get; set; }
        public string externalReference { get; set; }
        public bool sendCreditRequest { get; set; }
        public string customerOrgNo { get; set; }

        public SalesPerson salesPerson { get; set; }
        public IEnumerable<SalesPerson> salesPersons { get; set; }

        public string selectedCustomerOfferKey { get; set; }
        public CustomerOffer customerOffer { get; set; }
        public IEnumerable<CustomerOffer> customerOffers { get; set; }

        public Calculation calculation { get; set; }

        public CalculationResult calculationResult { get; set; }

        public PartnerInput partnerInput { get; set; }
    }
}