﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPITest.Models
{
    public class Calculation
    {
        public int duration { get; set; }

        public bool inAdvance { get; set; }

        public string interestType { get; set; }

        public int periodLength { get; set; }

        public string residualClause { get; set; }

        public int salesCommission { get; set; }

        public IEnumerable<ProductRow> productRows { get; set; }

        public string externalReference { get; set; }

        public bool smeFinancing { get; set; }

        public int noOfEmployees { get; set; }
    }
}