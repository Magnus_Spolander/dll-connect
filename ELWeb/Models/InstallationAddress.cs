﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebAPITest.Models
{
    public class InstallationAddress
    {
        public string address { get; set; }
        public string location { get; set; }
    }
}
