﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebAPITest.Models
{
    public class ProductSection
    {
        public string sectionID { get; set; }
        public string sectionName { get; set; }
        public IEnumerable<string> products { get; set; }
    }
}
