﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPITest.Models
{
    public class ProductRow
    {
        public string type { get; set; }
        public string name { get; set; }
        public string specification { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }
        public decimal rent { get; set; }
    }
}