﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPITest.Models
{
    public class CreditIndexViewModel
    {
        public string dealID { get; set; }
        public string externalReference { get; set; }
        public string creditDecision { get; set; }
        public string creditDecisionDate { get; set; }
        public string messageToCredit { get; set; }
        public int financedAmount { get; set; }
        public int duration { get; set; }
        public string customerOfferName { get; set; }
        public string customerOfferID { get; set; }
        public string salesPersonName { get; set; }
        public bool sendToManualEvaluation { get; set; }
        public bool sendCreditRequest { get; set; }
        public string customerOrgNo { get; set; }
        public Customer customer { get; set; }
    }
}