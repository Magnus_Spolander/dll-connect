﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebAPITest.Models
{
    public class LoginInfo
    {
        public string sessionID { get; set; }
        public string countryCode { get; set; }
    }
}
