﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebAPITest.Models
{
    public class Customer
    {
        public string organizationNo { get; set; }
        public string contact { get; set; }
        public string customerName { get; set; }
        public string coName { get; set; }
        public string postalAddress { get; set; }
        public string city { get; set; }
        public string zipCode { get; set; }
    }
}
