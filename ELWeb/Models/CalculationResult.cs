﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebAPITest.Models
{
    public class CalculationResult
    {
        public int periodLength { get; set; }
        public int financedAmount { get; set; }
        public decimal periodFee { get; set; }
        public int insuranceFee { get; set; }
        public int residualValue { get; set; }
        public decimal extensionFee { get; set; }
        public decimal serviceFee { get; set; }
        public decimal accServiceFees { get; set; }
        public decimal smeDiscount { get; set; }
    }
}
