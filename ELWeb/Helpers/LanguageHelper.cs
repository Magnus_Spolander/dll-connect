﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebAPITest.Models;

namespace WebAPITest.Helpers
{
    public class LanguageHelper
    {
        public static CultureInfo CreateFromCountryCode(string countryCode)
        {
            
            switch (countryCode)
            {
                case "SE":
                    return CultureInfo.CreateSpecificCulture("sv-SE");
                case "DK":
                    return CultureInfo.CreateSpecificCulture("da-DK");
                case "NO":
                    return CultureInfo.CreateSpecificCulture("nn-NO");
                case "FI":
                    return CultureInfo.CreateSpecificCulture("fi-FI");
                case "EN":
                    return CultureInfo.CreateSpecificCulture("en-GB");
            }
            
            // all other cultures, the default resource file is for english which will be used if insent country code does not exist as resource file
            return CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.Name.EndsWith(countryCode)).FirstOrDefault();
        }

        public static void ChangeLanguage(string language)
        {
            SessionObjects.currentCulture = new CultureInfo(language);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
        }


        public static IEnumerable<SelectListItem> AvailableLanguages
        {
            get
            {
                var currentUICulture = Thread.CurrentThread.CurrentUICulture.Name;
                List<SelectListItem> availableLanguages = new List<SelectListItem>();
                availableLanguages.Add(new SelectListItem() { Text = "Dansk", Value = "da-DK", Selected = currentUICulture  == "da-DK"});
                availableLanguages.Add(new SelectListItem() { Text = "English", Value = "en-GB", Selected = (currentUICulture != "da-DK" && currentUICulture != "nn-NO" && currentUICulture != "sv-SE") }); // this is the default fallback language so that will be selected if the language does not exist as resource
                availableLanguages.Add(new SelectListItem() { Text = "Norsk", Value = "nn-NO", Selected = currentUICulture == "nn-NO" });
                availableLanguages.Add(new SelectListItem() { Text = "Svenska", Value = "sv-SE", Selected = currentUICulture == "sv-SE" });


                return availableLanguages.AsEnumerable();
                //availableLanguages.Add(new SelectListItem() { Text = "Dansk", Value = "da-DK"});
                //availableLanguages.Add(new SelectListItem() { Text = "English", Value = "en-GB"});
                //availableLanguages.Add(new SelectListItem() { Text = "Norsk", Value = "nn-NO"});
                //availableLanguages.Add(new SelectListItem() { Text = "Svenska", Value = "sv-SE"});

                
                //return availableLanguages.Select( l => new SelectListItem { Selected = (Thread.CurrentThread.CurrentUICulture.Name == l.Value), Value = l.Value, Text = l.Text}) ;
            }        
        }
    }
}