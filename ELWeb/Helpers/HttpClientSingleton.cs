﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace WebAPITest.Helpers
{
    public sealed class HttpClientSingleton
    {
        private static HttpClient _instance = null;

        public static HttpClient Instance
        {
            get
            {
                if (_instance == null)
                {
                    HttpClientHandler handler = new HttpClientHandler();
                    handler.UseProxy = false;
                    _instance = new HttpClient(handler);
                    _instance.BaseAddress = new Uri(ConfigurationManager.AppSettings["EntreBaseAddress"]);
                }
                return _instance;
            }
        }
    }
}