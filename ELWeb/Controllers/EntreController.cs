﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPITest.Models;
using WebAPITest.Entre;
using WebAPITest.Attributes;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web;
using System.Web.Security;

namespace WebAPITest.Controllers
{
    public class EntreController : ApiController
    {
        EntreProxy entre = new EntreProxy();

        [HttpPost]
        [ActionName("GetPartnerData")]
        public HttpResponseMessage GetPartnerData()
        {
            EntreMessage<PartnerData> response = entre.GetPartnerData(new { externalSessionID = SessionObjects.CurrentSessionId });
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [ActionName("Calculate")]
        public HttpResponseMessage Calculate(JObject input)
        {
            input["externalSessionID"] = SessionObjects.CurrentSessionId;
            EntreMessage<CalculationResult> response = entre.Calculate(input);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [ActionName("SaveCalculation")]
        public HttpResponseMessage SaveCalculation(JObject input)
        {
            input["externalSessionID"] = SessionObjects.CurrentSessionId;
            EntreMessage<JObject> response = entre.SaveCalculation(input);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [ActionName("GetCustomer")]
        public HttpResponseMessage GetCustomer(JObject input)
        {
            input["externalSessionID"] = SessionObjects.CurrentSessionId;
            EntreMessage<JObject> response = entre.GetCustomer(input);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [ActionName("SaveCustomer")]
        public HttpResponseMessage SaveCustomer(JObject input)
        {
            input["externalSessionID"] = SessionObjects.CurrentSessionId;
            EntreMessage<JObject> response = entre.SaveCustomer(input);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [ActionName("SendCreditRequest")]
        public HttpResponseMessage SendCreditRequest(JObject input)
        {
            input["externalSessionID"] = SessionObjects.CurrentSessionId;
            EntreMessage<JObject> response = entre.SendCreditRequest(input);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [ActionName("GetAgreementData")]
        public HttpResponseMessage GetAgreementData(JObject input)
        {
            input["externalSessionID"] = SessionObjects.CurrentSessionId;
            EntreMessage<JObject> response = entre.GetAgreementData(input);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [ActionName("CreateAgreement")]
        public HttpResponseMessage CreateAgreement(JObject input)
        {            
            input["externalSessionID"] = SessionObjects.CurrentSessionId;
            EntreMessage<JObject> response = entre.CreateAgreement(input);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [ActionName("GetDealByReferenceNo")]
        public HttpResponseMessage GetDealByReferenceNo(JObject input)
        {
            input["externalSessionID"] = SessionObjects.CurrentSessionId;
            EntreMessage<JObject> response = entre.GetDealByReferenceNo(input);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [ActionName("LogOut")]
        public HttpResponseMessage LogOut()
        {
            try
            {
                SessionObjects.ClearAllSessionData();
                FormsAuthentication.SignOut();
                HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, new EntreMessage<JObject>());
        }
        
        [HttpPost]
        [ActionName("SendEsign")]
        public HttpResponseMessage SendEsign(JObject input)
        {
            try
            {                
                var agreementEmail = input["agreementEmail"].AsEnumerable().Where(item => item.ToString().Length != 0);
                var deliveryApprovalEmail = input["deliveryApprovalEmail"].AsEnumerable().Where(item => item.ToString().Length !=0);                
                List<string> dealIDParts = input["dealID"].ToString().Split('-').Select(p => p.Trim()).ToList();

                if (agreementEmail.Count() == 0 && deliveryApprovalEmail.Count() == 0)
                {
                    throw new System.ArgumentException();
                }

                if (agreementEmail.Any(item => !item.ToString().Contains("@")) || deliveryApprovalEmail.Any(item => !item.ToString().Contains("@")))
                {
                    throw new System.FormatException();
                }

                input["externalSessionID"] = SessionObjects.CurrentSessionId;
                input["dealNo"] = dealIDParts.Count() > 1 ? string.Join("-", dealIDParts.ToArray(), 0, dealIDParts.Count() - 1) : dealIDParts[0]; 
                input["version"] = dealIDParts.Last();
                EntreMessage<JObject> response = entre.SendEsign(input);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (ArgumentException)
            {
                EntreMessage<JObject> missingEmail = new EntreMessage<JObject>() { code = 600, message = "", body = null };
                return Request.CreateResponse(HttpStatusCode.OK, missingEmail);
            }
            catch (FormatException)
            {
                EntreMessage<JObject> invalidEmail = new EntreMessage<JObject>() { code = 610, message = "", body = null };
                return Request.CreateResponse(HttpStatusCode.OK, invalidEmail);
            }
            catch (Exception ex)
            {
                EntreMessage<JObject> otherError = new EntreMessage<JObject>() { code = 999, message = ex.Message, body = null };
                return Request.CreateResponse(HttpStatusCode.OK, otherError);
            }
            
        }                
    }
}
