﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAPITest.Models;
using WebAPITest.Helpers;
using System.Web.Script.Serialization;
using System.ComponentModel.DataAnnotations;
using WebAPITest.Entre;
using System.Reflection;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.ComponentModel;
using ELWeb.Helpers;
using System.Threading;
using System.Web.Security;
using System.Configuration;

namespace WebAPITest.Controllers
{
    public class HomeController : Controller
    {
        [HttpPost]
        public ActionResult InputTest(PartnerInputRaw input)
        {
            try
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();

                PartnerInputAllStrings pi = jss.Deserialize<PartnerInputAllStrings>(input.inputData);
                dynamic checkObject = jss.DeserializeObject(input.inputData);

                // ERRORS
                List<string> errors = new List<string>();

                foreach (var item in typeof(PartnerInput).GetProperties())
                {
                    // strings
                    if (item.PropertyType.Name == "String")
                    {
                        foreach (var prop in checkObject)
                        {
                            if (prop.Key.ToUpper() == item.Name.ToUpper())
                            {
                                if (prop.Value.GetType().Name != typeof(string).Name)
                                {
                                    errors.Add(item.Name);
                                }
                            }
                        }
                    }
                    // booleans
                    else if (item.PropertyType.Name == "Boolean")
                    {
                        foreach (var prop in checkObject)
                        {
                            if (prop.Key.ToUpper() == item.Name.ToUpper())
                            {
                                if (prop.Value.GetType().Name != typeof(Boolean).Name)
                                {
                                    errors.Add(item.Name);
                                }
                            }
                        }
                    }
                    // integers
                    else if (item.PropertyType.Name == typeof(int).Name)
                    {
                        foreach (var prop in checkObject)
                        {
                            if (prop.Key.ToUpper() == item.Name.ToUpper())
                            {
                                if (prop.Value.GetType().Name != typeof(int).Name)
                                {
                                    errors.Add(item.Name);
                                }
                            }
                        }
                    }
                        // special cases (class in class)
                    else if (item.PropertyType.Name == "InvoiceAddress" || item.PropertyType.Name == "InstallationAddress" || item.PropertyType.Name == "CustomerInfo")
                    {
                        foreach (var prop in checkObject)
                        {
                            if (prop.Key.ToUpper() == item.Name.ToUpper())
                            {
                                foreach (var address in prop.Value)
                                {
                                    // should only be strings in those classes
                                    if (address.Value.GetType().Name != typeof(string).Name)
                                    {
                                        errors.Add(item.Name + "." + char.ToLower(address.Key[0]) + address.Key.Substring(1));
                                    }
                                }
                            }
                        }
                        
                    }
                    else if (item.Name == "products")
                    {
                        foreach (var prop in checkObject)
                        {
                            if (prop.Key.ToUpper() == item.Name.ToUpper())
                            {
                                int i = 1;
                                foreach (var product in prop.Value)
                                {
                                    foreach (var attribute in product)
                                    {
                                        // strings
                                        if (attribute.Key.ToLower() == "type" || attribute.Key.ToLower() == "name" || attribute.Key.ToLower() == "specification")
                                        {
                                            // should only be strings in those classes
                                            if (attribute.Value.GetType().Name != typeof(string).Name)
                                            {
                                                errors.Add(string.Format("product{0}." + char.ToLower(attribute.Key[0]) + attribute.Key.Substring(1), i.ToString()));
                                            }
                                        }
                                        // int
                                        if (attribute.Key.ToLower() == "quantity")
                                        {
                                            // should only be strings in those classes
                                            if (attribute.Value.GetType().Name != typeof(int).Name)
                                            {
                                                errors.Add(string.Format("product{0}.quantity", i.ToString()));
                                            }
                                        }
                                        // decimal
                                        if (attribute.Key.ToLower() == "price" || attribute.Key.ToLower() == "rent")
                                        {
                                            // should only be strings in those classes
                                            if (attribute.Value.GetType().Name != typeof(decimal).Name && attribute.Value.GetType().Name != typeof(int).Name)
                                            {
                                                errors.Add(string.Format("product{0}." + char.ToLower(attribute.Key[0]) + attribute.Key.Substring(1), i.ToString()));
                                            }
                                        }
                                    }
                                    i++;
                                }
                            }
                        }                        
                    }
                }

                List<string> warnings = new List<string>();

                //WARNINGS
                foreach (var item in typeof(PartnerInputAllStrings).GetProperties())
                {
                    if (!input.inputData.ToUpper().Contains(item.Name.ToUpper()) && (item.Name != "Errors" && item.Name != "Warnings"))
                    {
                        warnings.Add(item.Name);
                    }
                }

                // Check subclasses (we need to check each attribute because they share some names))
                if (pi.installationAddress != null)
                {
                    if (string.IsNullOrEmpty(pi.installationAddress.address))
                    {
                        warnings.Add("installationAddress.address");
                    }
                    if (string.IsNullOrEmpty(pi.installationAddress.location))
                    {
                        warnings.Add("installationAddress.location");
                    }

                }
                else
                {
                    pi.installationAddress = new InstallationAddress(); // create empty object
                }


                if (pi.invoiceAddress != null)
                {
                    if (string.IsNullOrEmpty(pi.invoiceAddress.address))
                    {
                        warnings.Add("invoiceAddress.address");
                    }
                    if (string.IsNullOrEmpty(pi.invoiceAddress.city))
                    {
                        warnings.Add("invoiceAddress.city");
                    }
                    if (string.IsNullOrEmpty(pi.invoiceAddress.coName))
                    {
                        warnings.Add("invoiceAddress.coName");
                    }
                    if (string.IsNullOrEmpty(pi.invoiceAddress.zipCode))
                    {
                        warnings.Add("invoiceAddress.zipCode");
                    }
                }
                else
                {
                    pi.invoiceAddress = new InvoiceAddress(); // create empty object
                }

                if (pi.customerInfo != null)
                {
                    if (string.IsNullOrEmpty(pi.customerInfo.contact))
                    {
                        warnings.Add("customerInfo.contact");
                    }
                    if (string.IsNullOrEmpty(pi.customerInfo.email))
                    {
                        warnings.Add("customerInfo.email");
                    }
                    if (string.IsNullOrEmpty(pi.customerInfo.fax))
                    {
                        warnings.Add("customerInfo.fax");
                    }
                    if (string.IsNullOrEmpty(pi.customerInfo.phoneNo))
                    {
                        warnings.Add("customerInfo.phoneNo");
                    }
                }
                else
                {
                    pi.customerInfo = new CustomerInfo(); // create empty object
                }

                // go through the products manually and check for both errors and warnings for each one
                if (pi.products != null)
                {
                    int i = 1;
                    foreach (var item in pi.products)
                    {
                        if (string.IsNullOrEmpty(item.name))
                        {
                            warnings.Add(string.Format("product{0}.name", i));
                        }
                        if (string.IsNullOrEmpty(item.price))
                        {
                            warnings.Add(string.Format("product{0}.price", i));
                        }
                        if (string.IsNullOrEmpty(item.quantity))
                        {
                            warnings.Add(string.Format("product{0}.quantity", i));
                        }
                        if (string.IsNullOrEmpty(item.rent))
                        {
                            warnings.Add(string.Format("product{0}.rent", i));
                        }
                        if (string.IsNullOrEmpty(item.specification))
                        {
                            warnings.Add(string.Format("product{0}.specification", i));
                        }
                        if (string.IsNullOrEmpty(item.type))
                        {
                            warnings.Add(string.Format("product{0}.type", i));
                        }
                        i++;
                    }
                }

                // get support data from Entre
                SetSupportData(pi.username, pi.password);

                pi.Errors = errors;
                pi.Warnings = warnings;

                return View("InputTest", pi);

            }
            catch (Exception ex)
            {
                Logger.GetLogger(typeof(HomeController)).Error("Failed to deserialize input!", ex);
                return View("CustomError", new CustomError() { ErrorTitle = "Invalid input", ErrorMessage = ex.Message });
            }

            
        }

        // we need to log in to know which country we have
        // if those information are not accesseble we leave them empty
        private void SetSupportData(string userName, string passWord)
        {
            try
            {
                EntreProxy proxy = new EntreProxy();
                Guid sessionId = new Guid();
                var login = proxy.Login(new { username = userName, password = passWord, externalSessionID = sessionId });
                Dictionary<string, string> supportEmails = new Dictionary<string, string> { { "SE", ConfigurationManager.AppSettings["SupportEmailSE"] }, { "DK", ConfigurationManager.AppSettings["SupportEmailDK"] }, { "NO", ConfigurationManager.AppSettings["SupportEmailNO"] }, { "FI", ConfigurationManager.AppSettings["SupportEmailFI"] } };
                Dictionary<string, string> supportPhones = new Dictionary<string, string> { { "SE", ConfigurationManager.AppSettings["SupportPhoneSE"] }, { "DK", ConfigurationManager.AppSettings["SupportPhoneDK"] }, { "NO", ConfigurationManager.AppSettings["SupportPhoneNO"] }, { "FI", ConfigurationManager.AppSettings["SupportPhoneFI"] } };

                if (login.body.countryCode != null)
                {
                    SessionObjects.CurrentSupportInfo.email = supportEmails[login.body.countryCode];
                    SessionObjects.CurrentSupportInfo.phoneNo = supportPhones[login.body.countryCode];
                }
                else // default, swedish
                {
                    SessionObjects.CurrentSupportInfo.email = supportEmails["SE"];
                    SessionObjects.CurrentSupportInfo.phoneNo = supportPhones["SE"];
                }
            }
            catch (Exception ex)
            {
                // only log the exception
                Logger.GetLogger(typeof(HomeController)).Error("Failed to log in to get countrycode for input test", ex);                
            }


            
        }

        [HttpPost]
        public ActionResult ChangeLanguage(string selectedValue)
        {
            try
            {
                Logger.GetLogger(typeof(HomeController)).DebugFormat("Changing language to {0}", selectedValue);
                Response.SetCookie(new HttpCookie("culture", selectedValue));
                LanguageHelper.ChangeLanguage(selectedValue);
                return Json(new { code = 0, message = "OK" });
            }
            catch (Exception ex)
            {
                Logger.GetLogger(typeof(HomeController)).Error(ex.Message);
                throw ex;
            }
        }
    }
}
