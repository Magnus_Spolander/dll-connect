﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAPITest.Entre;
using WebAPITest.Models;
using WebAPITest.Helpers;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Threading;
using System.Globalization;
using System.Diagnostics;
using System.Configuration;
using ELWeb.Helpers;
using System.Web.Security;
using System.Text.RegularExpressions;

namespace WebAPITest.Controllers
{
    public class CalculationController : Controller
    {
        private EntreProxy proxy = new EntreProxy();


        [HttpPost]
        public ActionResult Index(PartnerInputRaw rawInput)
        {
            PartnerInput pi;
            try
            {
                Logger.GetLogger(typeof(CalculationController)).Debug("Deserializing input...");
                Logger.GetLogger(typeof(CalculationController)).DebugFormat("Input {0}", !string.IsNullOrEmpty(rawInput.inputData) ? rawInput.inputData : "No input data!");
                // To help us with error traceing when rawInput is empty for some reason
                Logger.GetLogger(typeof(CalculationController)).DebugFormat("Browser version: '{0}'", Request.Browser.Version);
                Logger.GetLogger(typeof(CalculationController)).DebugFormat("Content type: '{0}'", Request.ContentType);

                if (string.IsNullOrEmpty(rawInput.inputData))
                {
                    byte[] requestData = Request.BinaryRead(Request.TotalBytes);
                    string requestString = System.Text.Encoding.UTF8.GetString(requestData);

                    Logger.GetLogger(typeof(CalculationController)).Debug("rawInput is empty, trying to fetch input from the request...");
                    Logger.GetLogger(typeof(CalculationController)).DebugFormat("The request: {0}", requestString);

                    if (!string.IsNullOrEmpty(requestString))
                    {
                        // Only split on first = and return the last record. This to remove the InputData= part of the request
                        rawInput.inputData = requestString.Split(new char[] { '=' }, 2, StringSplitOptions.RemoveEmptyEntries).Last();
                        Logger.GetLogger(typeof(CalculationController)).DebugFormat("Input fetched from the request: '{0}'", rawInput.inputData);
                    }
                    else
                    {
                        Logger.GetLogger(typeof(CalculationController)).Debug("The request is also empty, deserializing not possible...");
                        throw new Exception();
                    }
                }

                pi = PartnerInput.Deserialize(rawInput.inputData);
                SessionObjects.inputData = rawInput.inputData;

            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(CalculationController)).Error("Failed to deserialize input!", e);
                return View("CustomError", new CustomError() { ErrorTitle = "Invalid input", ErrorMessage = e.Message });
            }


            try
            {
                SessionObjects.CurrentUserName = pi.username;
                string sessionID = Guid.NewGuid().ToString();

                var cookie = new FormsAuthenticationTicket(
                    0,
                    SessionObjects.CurrentUserName,
                    DateTime.Now,
                    DateTime.Now.AddHours(2),
                    false,
                    sessionID,
                    FormsAuthentication.FormsCookiePath
                    );

                string encTicket = FormsAuthentication.Encrypt(cookie);
                Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));


                EntreMessage<LoginInfo> response = proxy.Login(new { username = pi.username, password = pi.password, externalSessionID = sessionID });
                if (response.code != 0)
                {
                    return View("CustomError", new CustomError() { ErrorTitle = "Could not log in", ErrorMessage = response.message });
                }

                // 1. if cookie already set we use that
                if (Request.Cookies.Get("culture") != null && Request.Cookies.Get("culture").Value != null)
                {
                    try
                    {
                        var ci = CultureInfo.CreateSpecificCulture(Request.Cookies.Get("culture").Value);
                        Thread.CurrentThread.CurrentCulture = ci;
                        Thread.CurrentThread.CurrentUICulture = ci;
                    }
                    catch (Exception ex)
                    {
                        Logger.GetLogger(typeof(CalculationController)).Error("Failed to set current culture!");
                        Logger.GetLogger(typeof(CalculationController)).ErrorFormat("Error message: '{0}'", ex.Message);
                    }

                }
                // 2. if user has sent in with his json string 
                else if (!string.IsNullOrEmpty(pi.countryCode))
                {
                    SetCurrentCulture(pi.countryCode);
                }
                // 3. use culture from Entre
                else if (response.body.countryCode != null)
                {
                    SetCurrentCulture(response.body.countryCode);
                }

                // set Entre country code so we know where to go if user wants to go to Entre
                Response.SetCookie(new HttpCookie("entre-country-code", response.body.countryCode == null ? "SE" : response.body.countryCode));


                Dictionary<string, string> supportEmails = new Dictionary<string, string> { { "SE", ConfigurationManager.AppSettings["SupportEmailSE"] }, { "DK", ConfigurationManager.AppSettings["SupportEmailDK"] }, { "NO", ConfigurationManager.AppSettings["SupportEmailNO"] }, { "FI", ConfigurationManager.AppSettings["SupportEmailFI"] } };
                Dictionary<string, string> supportPhones = new Dictionary<string, string> { { "SE", ConfigurationManager.AppSettings["SupportPhoneSE"] }, { "DK", ConfigurationManager.AppSettings["SupportPhoneDK"] }, { "NO", ConfigurationManager.AppSettings["SupportPhoneNO"] }, { "FI", ConfigurationManager.AppSettings["SupportPhoneFI"] } };
                SessionObjects.CurrentSupportInfo.email = supportEmails[response.body.countryCode];
                SessionObjects.CurrentSupportInfo.phoneNo = supportPhones[response.body.countryCode];


                EntreMessage<PartnerData> partnerData = proxy.GetPartnerData(new { externalSessionID = sessionID });

                response.body.sessionID = sessionID;


                SessionObjects.CurrentSalesPersonInfo.contact = partnerData.body.salesContact.name;
                SessionObjects.CurrentSalesPersonInfo.email = partnerData.body.salesContact.email;
                SessionObjects.CurrentSalesPersonInfo.phoneNo = partnerData.body.salesContact.phone;


                SessionObjects.SendToManual = partnerData.body.sendCreditToManual.value;
                SessionObjects.SendToManualEditable = partnerData.body.sendCreditToManual.editable;
                SessionObjects.PartnerOrderNumber = pi.externalReference;

                if (pi.invoiceAddress != null)
                {
                    SessionObjects.InvoiceAddressInfo.coName = pi.invoiceAddress.coName;
                    SessionObjects.InvoiceAddressInfo.postalAddress = pi.invoiceAddress.address;
                    SessionObjects.InvoiceAddressInfo.city = pi.invoiceAddress.city;
                    SessionObjects.InvoiceAddressInfo.zipCode = pi.invoiceAddress.zipCode;

                }
                if (pi.installationAddress != null)
                {
                    SessionObjects.InstallationAddress = pi.installationAddress.address;
                    SessionObjects.InstallationAddressLocation = pi.installationAddress.location;
                }
                if (pi.customerInfo != null)
                {
                    SessionObjects.CurrentCustomerInfo.contact = pi.customerInfo.contact;
                    SessionObjects.CurrentCustomerInfo.email = pi.customerInfo.email;
                    SessionObjects.CurrentCustomerInfo.fax = pi.customerInfo.fax;
                    SessionObjects.CurrentCustomerInfo.phoneNo = pi.customerInfo.phoneNo;
                }

                //EntreMessage<CalculationResult> resp = apiHelper.Calculate(new Calculation());
                CalculationIndexViewModel model = new CalculationIndexViewModel();

                //Quick fix for calculating by rent
                // See over!
                if (pi.calculateByRent)
                {
                    if (pi.duration > 0 && pi.periodLength > 0)
                    {
                        foreach (ProductRow row in pi.products)
                        {
                            if (row.rent > 0 && row.quantity > 0)
                            {
                                row.price = row.rent * (pi.duration / pi.periodLength);
                            }
                            else
                            {
                                row.price = 0;
                            }
                        }
                    }
                    else
                    {
                        return View("CustomError", new CustomError() { ErrorTitle = "Unable to calculate by rent", ErrorMessage = "Both duration and period length must be specified" });
                    }
                }

                model.partnerInput = pi;
                Logger.GetLogger(typeof(CalculationController)).Debug("Succeeded to deserialize input!");
                return View("Index", model);
            }
            catch (Exception ex)
            {
                Logger.GetLogger(typeof(CalculationController)).Error("Failed to initialize calculation controller!", ex);
                throw ex;
            }
        }

        private void SetCurrentCulture(string countryCode)
        {
            try
            {
                CultureInfo current = LanguageHelper.CreateFromCountryCode(countryCode);
                // if culture for insent countrycode was not found then we do not set the culture and computers default culture is used
                if (current != null)
                {
                    Thread.CurrentThread.CurrentCulture = current;
                    Thread.CurrentThread.CurrentUICulture = current;
                    Response.SetCookie(new HttpCookie("culture", current.Name));

                }            

            }
            catch (Exception ex)
            {

                Logger.GetLogger(typeof(CalculationController)).Error("Failed to set current culture!", ex);

            }
        }
    }
}
