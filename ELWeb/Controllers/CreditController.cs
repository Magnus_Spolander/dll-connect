﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAPITest.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using ELWeb.Helpers;

namespace WebAPITest.Controllers
{
    public class CreditController : Controller
    {
        //
        // GET: /Credit/

        public ActionResult Index()
        {
            // check if logged in
            var currentSession = SessionObjects.CurrentSessionId;

            CreditIndexViewModel model = new CreditIndexViewModel();
            
            model.customerOfferID = "DE595EC108C0D501C12575AF004A09CF";
            model.customerOfferName = "IT HYRA";
            model.dealID = "90397-1";
            model.duration = 24;
            model.externalReference = "REF123";
            model.financedAmount = 150000;
            model.salesPersonName = "Säljjohan";

            return View("Index", model);
        }


        [HttpPost]
        public ActionResult Index(CreditInput input)
        {
            try
            {
                Logger.GetLogger(typeof(CreditController)).Debug("Deserializing input...");
                CreditIndexViewModel model = JsonConvert.DeserializeObject<CreditIndexViewModel>(input.creditData);
                SessionObjects.EntreDealNumber = model.dealID;
                model.sendToManualEvaluation = SessionObjects.SendToManual;
                Logger.GetLogger(typeof(CreditController)).Debug("Succeeded to deserialize input!");
                return View("Index", model);
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(CreditController)).Error("Failed to deserialize input", e);
                throw;
            }
        }

    }
}
