﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAPITest.Models;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Net;
using ELWeb.Helpers;

namespace WebAPITest.Controllers
{
    public class AgreementController : Controller
    {
        [HttpPost]
        public ActionResult Index(AgreementInput input)
        {
            Logger.GetLogger(typeof(AgreementController)).Debug("Deserializing input...");
            try
            {
                // check if logged in
                var currentSession = SessionObjects.CurrentSessionId;
                AgreementIndexViewModel model = JsonConvert.DeserializeObject<AgreementIndexViewModel>(input.agreementData);
                model.invoiceCoName = SessionObjects.InvoiceAddressInfo.coName;
                model.invoiceAddress = SessionObjects.InvoiceAddressInfo.postalAddress;
                model.invoiceCity = SessionObjects.InvoiceAddressInfo.city;
                model.invoiceZipCode = SessionObjects.InvoiceAddressInfo.zipCode;

                model.installationAddress = SessionObjects.InstallationAddress;
                model.installationLocation = SessionObjects.InstallationAddressLocation;
                Logger.GetLogger(typeof(AgreementController)).Debug("Succeeded to deserialize input!");
                return View("Index", model);
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(AgreementController)).Error("Failed to deserialize input!", e);
                throw;
            }
        }

        [HttpPost]
        public FileStreamResult PDF(string id, string base64)
        {
            try
            {
                Logger.GetLogger(typeof(AgreementController)).Debug("Streaming pdf...");
                MemoryStream stream = new MemoryStream(Convert.FromBase64String(base64));
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + id + ".pdf");
                Logger.GetLogger(typeof(AgreementController)).Debug("Succeeded to stream pdf!");
                return new FileStreamResult(stream, "application/pdf");
            }
            catch (Exception e)
            {
                Logger.GetLogger(typeof(AgreementController)).Error("Failed to convert to pdf!", e);
                throw;
            }

        }

        [HttpPost]
        public ActionResult Mail(string id, MailInfo input)
        {
            try
            {
                Logger.GetLogger(typeof(AgreementController)).DebugFormat("Sending email to '{0}'...", input.to);
                MailMessage mail = new MailMessage(input.from, input.to, input.subject, input.body);
                if (input.attachment != "")
                {
                    MemoryStream stream = new MemoryStream(Convert.FromBase64String(input.attachment));
                    mail.Attachments.Add(new Attachment(stream, id + ".pdf", "application/pdf"));
                }
                SmtpClient smtp = new SmtpClient("dllrelay.delagelanden.com");//"berlin.mpab.se");//"mail2.bahnhof.se");
                smtp.Send(mail);
                Logger.GetLogger(typeof(AgreementController)).Debug("Email sent successfully!");
                return Json(new {code = 0, message = "OK"});
            }
            catch(Exception e)
            {
                Logger.GetLogger(typeof(AgreementController)).Error("Failed to send email!", e);
                return Json(new { code = 500, message = e.Message });
            }
        }
        
    }
}
