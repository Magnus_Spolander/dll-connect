﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http.Headers;
using System.Threading;
using System.Globalization;

namespace WebAPITest.Attributes
{
    public class LocalizationAttribute : ActionFilterAttribute, IActionFilter
    {

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpCookie cookie = filterContext.HttpContext.Request.Cookies.Get("culture");
            if (cookie != null && cookie.Value != null)
            {
                var ci = CultureInfo.CreateSpecificCulture(cookie.Value);
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;
            }
            this.OnActionExecuting(filterContext);
        }
    }
}