﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPITest.Attributes
{
    //Borde skrivas om som en Message Handler istället för att verifiera cookie innan requesten kommer in i controller context
    public class CustomAuthorizeAttribute : System.Web.Http.AuthorizeAttribute
    {

    }
}