﻿ko.bindingHandlers.booleanValue = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var observable = valueAccessor(),
                    interceptor = ko.computed({
                        read: function () {
                            return String(observable());
                        },
                        write: function (newValue) {
                            if (newValue === true || newValue === "true") {
                                observable(true);
                            }
                            else if (newValue === false || newValue === "false") {
                                observable(false);
                            }
                            else {
                                observable(null);
                            }
                        }
                    });

        ko.applyBindingsToNode(element, { value: interceptor });
    }
};

ko.bindingHandlers.fixedValuesSlider = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var options = allBindingsAccessor().sliderOptions || {};
        $(element).slider(options);
        ko.utils.registerEventHandler(element, "slidechange", function (event, ui) {
            var values = ko.utils.unwrapObservable(allBindingsAccessor().customOptions.values) || [];
            var observable = valueAccessor();
            observable(values[ui.value]);
        });
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).slider("destroy");
        });
        ko.utils.registerEventHandler(element, "slide", function (event, ui) {
            var values = ko.utils.unwrapObservable(allBindingsAccessor().customOptions.values) || [];
            var observable = valueAccessor();
            observable(values[ui.value]);
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var values = ko.utils.unwrapObservable(allBindingsAccessor().customOptions.values) || [];
        if (isNaN(value)) {
            value = 0;
        }
        else {
            value = Number(value);
        }
        $(element).slider("option", "max", values.length - 1);
        $(element).slider("value", $.inArray(value, values));
    }
};

ko.bindingHandlers.spinner = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().spinnerOptions || {};
        $(element).spinner(options);

        //handle the field changing
        ko.utils.registerEventHandler(element, "spinchange", function () {
            var observable = valueAccessor();
            observable($(element).spinner("value"));
        });

        ko.utils.registerEventHandler(element, "spinstop", function () {
            var observable = valueAccessor();
            observable($(element).spinner("value"));
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).spinner("destroy");
        });

    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());

        current = $(element).spinner("value");
        if (value !== current) {
            $(element).spinner("value", value);
        }
    }
};

ko.bindingHandlers.chosen = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        $(element).chosen(valueAccessor());
        $(element).trigger("liszt:updated");
    }
}