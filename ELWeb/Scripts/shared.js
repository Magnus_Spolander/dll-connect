﻿$.ajaxSetup({
    contentType: 'application/json',
    type: "POST",
    dataType: 'json',
    error: function (jqXHR, exception) {
        var message = "";
        //Removed because xhr.abort triggers the error function with status 0 and we do not want to display an error message on abort.
        //if (jqXHR.status === 0) {
        //    message = 'Not connected. Verify Network.';
        //} else
        if (jqXHR.status == 404) {
            message = 'Requested page not found. [404]';
        } else if (jqXHR.status == 500) {
            message = 'Internal Server Error [500].';
        } else if (exception === 'parsererror') {
            message = 'Requested JSON parse failed.';
        } else if (exception === 'timeout') {
            message = 'Time out error.';
        //} else if (exception === 'abort') {
        //    message = 'Ajax request aborted.';
        //} else {
        //    message = jqXHR.responseText;
        }
        if (message) {
            ajaxError(message);
        }
    }
});

function validationError(message, title) {
    //toastr.options = {
    //    positionClass: "toast-top-right",
    //    timeOut: 10000
    //}
    //toastr.error(message, title);
    alert(message);
}

function ajaxError(message, title) {
    toastr.options = {
        positionClass: "toast-top-center",
        timeOut: 2000,
        extendedTimeOut: 10000
    }
    toastr.error(message, title);
}

function successMessage(message, title) {
    toastr.options = {
        positionClass: "toast-top-center",
        timeOut: 2000,
        extendedTimeOut: 10000
    }
    toastr.success(message, title);
}

function alertObject(o) {
    var out = '';
    for (var p in o) {
        out += p + ': ' + o[p] + '\n';
    }
    alert(out);
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}



function gotoEntre() {
    $.ajax({
        contentType: 'application/json',
        type: "POST",
        url: '/api/entre/LogOut',
    });


    window.location = "http://www.delagelandenfinans." + readCookie("entre-country-code");
}